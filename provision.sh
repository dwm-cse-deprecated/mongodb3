#! /bin/bash

set -ex

target_dir=`cd $(dirname ${0}) && pwd`

cd /usr/local/src
wget https://fastdl.mongodb.org/linux/mongodb-linux-x86_64-3.2.3.tgz
tar zxvf mongodb-linux-x86_64-3.2.3.tgz
ln -s /usr/local/src/mongodb-linux-x86_64-3.2.3 /usr/local/mongodb

cp ${target_dir}/configs/initd /etc/rc.d/init.d/mongod
cp ${target_dir}/configs/mongod.conf /etc/mongod.conf
chmod 755 /etc/rc.d/init.d/mongod

groupadd mongod
useradd mongod -g mongod -d /var/lib/mongo -s /bin/false -M

mkdir /var/{run,log}/mongodb
mkdir /var/lib/mongo

chown mongod:mongod -R /var/{run,log}/mongodb
chown mongod:mongod -R /var/lib/mongo

chkconfig --add mongod

echo "export PATH=\$PATH:/usr/local/mongodb/bin" > /etc/profile.d/mongodb.sh
